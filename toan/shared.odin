package toan

import "core:intrinsics"

union_to :: #force_inline proc($T: typeid, u: ^$U) -> T
    where intrinsics.type_is_variant_of(U, T)
{
    return ((^T)(u))^
}


tval_to_f64 :: proc(v: ^TVal) -> (f64, bool) #optional_ok {

    v := v
    #partial switch t in v {
        case TInt   : return f64(union_to(i64, v)), true
        case TFloat : return union_to(f64, v), true
        case TBool  : return union_to(b64, v) ? 1 : 0, true
    }

    return 0, false
}

tval_to_i64 :: proc(v: ^TVal) -> (i64, bool) #optional_ok {
    #partial switch t in v {
        case TInt   : return union_to(i64, v), true
        case TFloat : return i64(union_to(f64, v)), true
        case TBool  : return union_to(b64, v) ? 1 : 0, true
    }

    return 0, false
}

tval_to_b64 :: proc(v: ^TVal) -> (b64, bool) #optional_ok {
    #partial switch t in v {
        case TInt: return union_to(i64, v) != 0, true
        case TFloat: return union_to(f64, v) != 0, true
        case TBool: return union_to(b64, v), true
    }

    return false, false
}
