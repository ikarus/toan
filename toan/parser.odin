package toan

import "core:fmt"
import "core:intrinsics"
_ :: intrinsics

Node :: struct {
    start: TokenPos,
    end: TokenPos,
    derived: NodeAny
}


File :: struct {
    using node: Node,
    path: string,
    data: []byte,

    stmts: [dynamic]^Stmt,
}


Param :: struct {
    using node: Node,
    name: ^Expr,
}

ParamList :: struct {
    using node: Node,
    open: TokenPos,
    list: []^Param,
    close: TokenPos,
}


Expr :: struct {
    using expr_base: Node,
    derived_expr: ExprAny,
}

ExprBad :: struct {
    using node: Expr,
    expr: ^Expr,
}

ExprIdent :: struct {
    using node: Expr,
    token: Token,
}

ExprLiteral :: struct {
    using node: Expr,
    is_neg: bool,
    token: Token,
}

ExprUnary :: struct {
    using node: Expr,
    op: Token,
    op_type: UnaryOpType,
    expr: ^Expr,
}

ExprBinary :: struct {
    using node: Expr,
    op: Token,
    op_type: BinaryOpType,
    lhs, rhs: ^Expr,
}

ExprFunc :: struct {
    using node: Expr,
    type: ^ExprProtoFunc,
    body: ^Stmt
}

ExprProtoFunc :: struct {
    using node: Expr,
    token: Token,
    params: ^ParamList,
}

ExprFuncCall :: struct {
    using node: Expr,
    token: Token,
    func: ^Expr,
    params: ^ExprList,
}

ExprList :: struct {
    using node: Expr,
    list: []^Expr,
}


Stmt :: struct {
    using stmt_base: Node,
    derived_stmt: StmtAny,
}

StmtEmpty :: struct { using node: Stmt }

StmtBad :: struct { using node: Stmt }

StmtBlock :: struct {
    using node: Stmt,
    open, close: TokenPos,
    stmts: []^Stmt
}

StmtAssign :: struct {
    using node: Stmt,
    op: Token,
    lhs: ^Expr,
    val: ^Expr,
}

StmtEval :: struct {
    using node: Stmt,
    expr: ^Expr,
}

StmtReturn :: struct {
    using node: Stmt,
    res: ^Expr,
}

StmtIf :: struct {
    using Node: Stmt,
    pred: ^Expr,
    body: ^Stmt,
    else_stmt: ^Stmt,
}

StmtPrint :: struct {
    using Node: Stmt,
    expr: ^Expr,
    ident_name: string,
}

NodeAny :: union {
    ^StmtEmpty,
    ^StmtBad,
    ^StmtAssign,
    ^StmtEval,
    ^StmtReturn,
    ^StmtBlock,
    ^StmtIf,
    ^StmtPrint,

    ^ExprBad,
    ^ExprLiteral,
    ^ExprIdent,
    ^ExprBinary,
    ^ExprUnary,
    ^ExprFunc,
    ^ExprProtoFunc,
    ^ExprFuncCall,
    ^ExprList,

    ^Param,
    ^ParamList,
    ^File,
}

ExprAny :: union {
    ^ExprBad,
    ^ExprLiteral,
    ^ExprIdent,
    ^ExprFunc,
    ^ExprBinary,
    ^ExprUnary,
    ^ExprProtoFunc,
    ^ExprFuncCall,
    ^ExprList,
}

StmtAny :: union {
    ^StmtEmpty,
    ^StmtBad,
    ^StmtAssign,
    ^StmtEval,
    ^StmtReturn,
    ^StmtBlock,
    ^StmtIf,
    ^StmtPrint,
}


// NOTE: maybe this can just be a bitset
BinaryOpType :: enum {
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Pow,
    Shl,
    Shr,
    And,
    Or,

    CompAnd,
    CompOr,
    CompEq,
    CompNeq,
    CompLt,
    CompLtEq,
    CompGt,
    CompGtEq,
}

UnaryOpType :: enum {
    Pos,
    Neg,
    Not,
    BitwiseNot,
}

Parser :: struct {
    tokenizer: Tokenizer,
    cur_token, prev_token: Token,
    error_count: int,
}

new_node :: proc($T: typeid, start, end: TokenPos) -> ^T {
    node := new(T);

    node.start = start
    node.end = end
    node.derived = node

    when intrinsics.type_has_field(T, "derived_expr") {
        node.derived_expr = node
    }

    when intrinsics.type_has_field(T, "derived_stmt") {
        node.derived_stmt = node
    }

    return node
}

report_parse_error :: proc(
    parser: ^Parser,
    _fmt: string,
    args: ..any,
    at := TokenPos{},
) {

    at := at
    if at.file == "" {
        at.file = parser.tokenizer.file
        at.line = parser.tokenizer.line
        at.col  = parser.tokenizer.col
    }

    parser.error_count +=1
    fmt.printf("%s(%d:%d) ", at.file, at.line, at.col)
    fmt.printf(_fmt, ..args)
    fmt.printf("\n")
}

report_bad_expr :: proc(parser: ^Parser, start_pos, end_pos: TokenPos, _fmt: string, args: ..any) -> ^Expr {
    report_parse_error(parser, _fmt, ..args)
    return new_node(ExprBad, start_pos, end_pos)
}

peek_token :: proc(parser: ^Parser, peek_by: int = 1) -> Token {

    peek_tokenizer := parser.tokenizer
    peek := parser.cur_token

    for i in 0..<peek_by {
        peek = get_token(&peek_tokenizer)
        if peek.type == .Eos {
            break
        }
    }

    return peek
}

adv_token :: proc(parser: ^Parser) -> Token {
    prev := parser.cur_token
    parser.prev_token = parser.cur_token
    parser.cur_token = get_token(&parser.tokenizer)


    return prev
}

skip_comment :: #force_inline proc(parser: ^Parser) -> Token {

    prev := parser.cur_token
    for parser.cur_token.type == .Comment {
        adv_token(parser)
    }

    return prev
}

token_precedence :: proc(token_type: TokenType) -> int {

    #partial switch token_type {
        case .CompEq, .CompNeq,
             .CompLt, .CompLtEq,
             .CompGt, .CompGtEq:
             return 4
        case .Add, .Sub, .Or:
            return 5
        case .Mul, .Div, .Mod,
            .And, .Shl, .Shr:
            return 6
        case .Hat:
            return 7
    }
    return 0
}

parse_func_proto :: proc (using parser: ^Parser) -> ^ExprProtoFunc {
    start := cur_token
    adv_token(parser)

    open := cur_token
    if open.type != .OpenParen {
        report_parse_error(parser, "Expected an open parenthesis, got %s", cur_token.text)
        return nil
    }

    params := make([dynamic]^Param, 0, 100)
    param_ok := true
    adv_token(parser)

    for cur_token.type != .Eos && param_ok {
        if cur_token.type == .Identifier {
            ident := new_node(ExprIdent, cur_token.pos, cur_token.pos)
            ident.token = cur_token
            param := new_node(Param, cur_token.pos, cur_token.pos)
            param.name = ident
            append(&params, param)
        } else {
            report_parse_error(parser, "Expected an identifier, got %s", cur_token.text)
            param_ok = false
        }

        adv_token(parser)

        if cur_token.type == .CloseParen {
            break
        }

        if cur_token.type != .Comma {
            report_parse_error(parser, "Expected a comma, got %s", cur_token.text)
            param_ok = false
        }
        adv_token(parser)
    }

    param_ok &= cur_token.type == .CloseParen

    if !param_ok {
        delete(params)
        return nil
    }

    close := cur_token
    param_list := new_node(ParamList, open.pos, close.pos)
    param_list.open = open
    param_list.close = close

    param_list.list = params[:]

    func_proto := new_node(ExprProtoFunc, start.pos, param_list.end)
    func_proto.token = start
    func_proto.params = param_list
    adv_token(parser)
    return func_proto
}

parse_func_body :: proc (using parser: ^Parser) -> ^Stmt {

    if cur_token.type != .OpenBrace {
        report_parse_error(parser, "Expected an open brace, got %s", cur_token.text)
        return nil
    }

    adv_token(parser)
    return parse_stmt_block(parser)
}

parse_operand :: proc(using parser: ^Parser, is_lhs: bool) -> ^Expr {

    #partial switch cur_token.type {
        case .Identifier: {
            expr := new_node(ExprIdent, cur_token.pos, cur_token.pos)
            expr.token = cur_token
            adv_token(parser)
            return expr
        }

        case .Integer, .Float: {
            expr := new_node(ExprLiteral, cur_token.pos, cur_token.pos)
            expr.token = cur_token
            adv_token(parser)
            return expr
        }

        case .OpenParen: {
            open := cur_token
            adv_token(parser)
            expr := parse_expr(parser, false)

            if cur_token.type != .CloseParen {
                report_parse_error(parser, "Expected a close parenthesis, got a %s", cur_token.text)
                bad_expr := new_node(ExprBad, open.pos, cur_token.pos)
                bad_expr.expr = expr
                expr = bad_expr
            }
            adv_token(parser)
            return expr
        }

        case .Func: {
            proto := parse_func_proto(parser)
            body := parse_func_body(parser)

            func := new_node(ExprFunc, proto.start, body.end)
            func.type = proto
            func.body = body
            return func
        }
    }

    return nil
}

parse_sub_expr :: proc(using parser: ^Parser, val: ^Expr, is_lhs: bool) -> ^Expr {
    operand := val
    if operand == nil {
        operand = report_bad_expr(
            parser, cur_token.pos, cur_token.pos, "Expected an operand, got %s", cur_token.text)
    }


    #partial switch t in operand.derived_expr {
        case ^ExprIdent: {
            #partial switch cur_token.type {
                case .OpenParen: {
                    func_token := prev_token
                    open := cur_token
                    adv_token(parser)

                    params_ok := true

                    exprs := make([dynamic]^Expr, 0, 100)
                    for cur_token.type != .Eos && params_ok {

                        cur_expr := parse_expr(parser, false)
                        if cur_expr == nil {
                            params_ok = false
                            break
                        }
                        append(&exprs, cur_expr)

                        if cur_token.type == .CloseParen {
                            break
                        }

                        if cur_token.type != .Comma {
                            report_parse_error(parser, "Expected a comma, got %s", cur_token.text)
                            params_ok = false
                            break
                        }

                        adv_token(parser)
                    }

                    if cur_token.type != .CloseParen {
                        report_parse_error(parser, "Expected closing parenthesis, got %s", cur_token.text)
                        params_ok = false
                    }

                    if !params_ok {
                        report_parse_error(parser, "Error in parsing function parameters")
                        delete(exprs)
                        return nil
                    }

                    param_list := new_node(ExprList, open.pos, cur_token.pos)
                    param_list.list = exprs[:]

                    call_expr := new_node(ExprFuncCall, operand.start, param_list.end)
                    call_expr.token = func_token
                    call_expr.func = operand
                    call_expr.params = param_list
                    adv_token(parser)
                    operand = call_expr
                }
            }
        }
    }
    return operand
}

parse_unary_expr :: proc(using parser: ^Parser, is_lhs: bool) -> ^Expr {

    #partial switch (cur_token.type) {
        case .Add, .Sub, .Excl: {
            op := cur_token
            adv_token(parser)
            op_type: UnaryOpType
            #partial switch op.type {
                case .Add   : op_type = .Pos
                case .Sub   : op_type = .Neg
                case .Excl  : op_type = .Not
                case .Tilde : op_type = .BitwiseNot
                case:
                    return report_bad_expr(
                        parser, op.pos, op.pos, "expected a unary operator ('!' or '+' or '-'), got %s", op.text)
            }

            expr := parse_unary_expr(parser, is_lhs)
            unary_expr := new_node(ExprUnary, op.pos, expr.end)
            unary_expr.op = op
            unary_expr.op_type = op_type
            unary_expr.expr = expr
            return unary_expr
        }
    }

    operand := parse_operand(parser, is_lhs)
    return parse_sub_expr(parser, operand, is_lhs)
}

parse_binary_expr :: proc(using parser: ^Parser, is_lhs: bool, outer_prec: int) -> ^Expr {

    start := cur_token
    expr := parse_unary_expr(parser, is_lhs)

    if expr == nil {
        return report_bad_expr(parser, start.pos, start.pos, "Expected expression, got %s", start.text)
    }

    for inner_prec := token_precedence(cur_token.type); inner_prec >= outer_prec; inner_prec -= 1 {
        inner_prec := inner_prec
        bin_loop : for {
            op := cur_token
            op_prec := token_precedence(op.type)

            if op_prec != inner_prec { break bin_loop }

            adv_token(parser)
            rhs := parse_binary_expr(parser, false, inner_prec + 1)
            if rhs == nil {
                return report_bad_expr(parser, op.pos, op.pos, "expected expression on the right, got %s", op.text)
            }

            bin_expr := new_node(ExprBinary, expr.start, rhs.end)
            bin_expr.lhs = expr
            bin_expr.op = op
            bin_expr.rhs = rhs

            #partial switch (op.type) {
                case : assert(false)

                case .Add           : bin_expr.op_type = .Add
                case .Sub           : bin_expr.op_type = .Sub
                case .Mul           : bin_expr.op_type = .Mul
                case .Div           : bin_expr.op_type = .Div
                case .Mod           : bin_expr.op_type = .Mod
                case .Hat           : bin_expr.op_type = .Pow
                case .Shl           : bin_expr.op_type = .Shl
                case .Shr           : bin_expr.op_type = .Shr
                case .And           : bin_expr.op_type = .And
                case .Or            : bin_expr.op_type = .Or
                case .CompEq        : bin_expr.op_type = .CompEq
                case .CompNeq       : bin_expr.op_type = .CompNeq
                case .CompLt        : bin_expr.op_type = .CompLt
                case .CompLtEq      : bin_expr.op_type = .CompLtEq
                case .CompGt        : bin_expr.op_type = .CompGt
                case .CompGtEq      : bin_expr.op_type = .CompGtEq
                case .CompAnd       : bin_expr.op_type = .CompAnd
                case .CompOr        : bin_expr.op_type = .CompOr

            }
            expr = bin_expr
        }
    }

    return expr
}

parse_expr :: proc(using parser: ^Parser, is_lhs: bool) -> ^Expr {
    return parse_binary_expr(parser, is_lhs, 1)
}

parse_stmt_block :: proc(using parser: ^Parser) -> ^Stmt {

    assert(prev_token.type == .OpenBrace)
    open := prev_token

    stmt_list := make([dynamic]^Stmt, 0, 100)
    stmts_ok := true

    for cur_token.type != .Eos && cur_token.type != .CloseBrace && stmts_ok {

        cur_stmt := parse_stmt(parser)

        if cur_stmt == nil {
            stmts_ok = false

        } else {
            _, is_bad := cur_stmt.derived_stmt.(^StmtBad)
            stmts_ok = !is_bad
        }

        append(&stmt_list, cur_stmt)
    }

    if cur_token.type != .CloseBrace {
        report_parse_error(parser, "missing close brace")
        report_parse_error(parser, "opening brace here", open.pos)
        stmts_ok = false
    }

    if !stmts_ok {
        report_parse_error(parser, "Error parsing statement block")
        delete(stmt_list)
        return new_node(StmtBad, open.pos, cur_token.pos)
    }

    block := new_node(StmtBlock, open.pos, cur_token.pos)
    block.open = open.pos
    block.close = cur_token.pos
    block.stmts = stmt_list[:]
    adv_token(parser)
    return block
}

parse_stmt :: proc(using parser: ^Parser) -> ^Stmt {

    skip_comment(parser)

    if cur_token.type == .Eos {
        return new_node(StmtEmpty, cur_token.pos, cur_token.pos)

    }

    if is_token_literal(cur_token) {
        peek := peek_token(parser)

        if peek.line == cur_token.line {
            #partial switch peek.type {
                case .EqOpBegin..=.EqOpEnd: {
                    op := peek
                    lhs := parse_expr(parser, true)
                    adv_token(parser)
                    val := parse_expr(parser, false)
                    stmt := new_node(StmtAssign, lhs.start, val.end)

                    stmt.op = op

                    stmt.lhs = lhs
                    stmt.val = val

                    return stmt
                }

                case .Add..=.OperatorEnd: {
                    val := parse_expr(parser, false)
                    stmt := new_node(StmtEval, val.start, val.end)
                    stmt.expr = val

                    return stmt
                }

                case : {
                    report_parse_error(parser, "Expected an operator, got %s", cur_token.text)
                    stmt := new_node(StmtBad, cur_token.pos, cur_token.pos)

                    return stmt
                }
            }

        } else {
            val := parse_expr(parser, false)
            stmt := new_node(StmtEval, val.start, val.end)
            stmt.expr = val
            return stmt
        }

    } else if is_token_operator(cur_token) {

        #partial switch cur_token.type {
            case .Eq: {
                report_parse_error(parser, "Assignment needs a name on the left")
                stmt := new_node(StmtBad, cur_token.pos, cur_token.pos)
                return stmt
            }

            case .OpenBrace: {
                adv_token(parser)
                return parse_stmt_block(parser)
            }
        }

        start := prev_token
        expr := parse_expr(parser, false)
        stmt := new_node(StmtEval, start.pos, expr.end)
        stmt.expr = expr

        return stmt
    } else if is_token_keyword(cur_token) {

        #partial switch cur_token.type {
            case .Return: {
                start := cur_token
                adv_token(parser)
                expr := parse_expr(parser, false)
                stmt := new_node(StmtReturn, start.pos, expr.end)
                stmt.res = expr

                return stmt
            }

            case .If: {
                start := cur_token
                adv_token(parser)
                pred := parse_expr(parser, false)

                if cur_token.type == .OpenBrace {
                    adv_token(parser)

                    body := parse_stmt_block(parser)
                    if_stmt := new_node(StmtIf, start.pos, cur_token.pos)

                    if cur_token.type == .Else {
                        adv_token(parser)
                        else_stmt := parse_stmt(parser)
                        if_stmt.else_stmt = else_stmt
                    }

                    if_stmt.body = body
                    if_stmt.pred = pred
                    return if_stmt

                } else {
                    report_parse_error(parser, "Expected an open brace after if expression, got %s", cur_token.text)
                    return new_node(StmtBad, start.pos, cur_token.pos)
                }
            }

            case .Print: {
                start := cur_token
                adv_token(parser)

                expr := parse_expr(parser, false)
                print := new_node(StmtPrint, start.pos, expr.end)
                print.expr = expr
                return print
            }

            case .PrintIdent: {
                start := cur_token
                adv_token(parser)

                expr := parse_expr(parser, false)
                print := new_node(StmtPrint, start.pos, expr.end)

                maybe_ident, is_ident := expr.derived_expr.(^ExprIdent)

                print.ident_name = string(maybe_ident.token.text)
                print.expr = expr
                return print
            }
        }
    }

    return nil
}


parse_file :: proc(using parser: ^Parser, file: ^File) -> bool {
    parser.tokenizer.file = file.path
    parser.tokenizer.stream = file.data

    adv_token(parser)

    file_ok := true
    for cur_token.type != .Eos && cur_token.type != .Invalid && file_ok {
        stmt := parse_stmt(parser)

        if stmt == nil {
            file_ok = false
        } else {
            _, is_bad := stmt.derived_stmt.(^StmtBad)
            file_ok = !is_bad
        }

        if file_ok {
            append(&file.stmts, stmt)
        }
    }

    return file_ok
}

