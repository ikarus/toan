package toan

import "core:fmt"
import "core:math"

StackFrame :: struct {
    ret: int,
    depth: int,
}

Interpreter :: struct {
    file_scope : Scope,
    current_scope : ^Scope,
    scopes: [dynamic]^Scope,

    call_stack: [256]StackFrame,
    call_stack_ptr: int,

    parser: Parser,
    current_file: ^File,

    error_count: int,
    errors: [20]EvalError,
}


Scope :: struct {
    lookup: map[string]TVal,
    level: int,
    parent: ^Scope,
}

TNil   :: struct {}
TInt   :: i64
TFloat :: f64
TBool  :: b64
TFunc  :: ^ExprFunc

TVal :: union {
    TNil,
    TInt,
    TFloat,
    TBool,
    TFunc,
}

EvalError :: struct {
    pos: TokenPos,
    err_msg: string,
    _err_msg_buf: [256]byte
}

report_eval_error :: proc (interp: ^Interpreter, pos: TokenPos, _fmt: string, args: ..any) {

    if interp.error_count < len(interp.errors) {
        error := &interp.errors[interp.error_count]
        error.pos = pos


        offset := 0
        err_msg := fmt.bprintf(error._err_msg_buf[offset:], "%s(%d:%d) ", pos.file, pos.line, pos.col)
        offset += len(err_msg)

        err_msg = fmt.bprintf(error._err_msg_buf[offset:], _fmt, ..args)
        offset += len(err_msg)

        err_msg = fmt.bprintf(error._err_msg_buf[offset:], "\n")
        offset += len(err_msg)

        error.err_msg = string(error._err_msg_buf[:offset])
        interp.error_count += 1
    }
}

eval_add :: proc(lhs, rhs: TVal) -> TVal {

    lhs := lhs
    rhs := rhs

    _, lhs_is_func := lhs.(TFunc)
    _, rhs_is_func := rhs.(TFunc)
    assert(!lhs_is_func)
    assert(!rhs_is_func)

    lhs_f, lhs_is_float := lhs.(TFloat)
    rhs_f, rhs_is_float := rhs.(TFloat)

    if lhs_is_float && rhs_is_float {
        return lhs_f + rhs_f
    } else if lhs_is_float {
        return lhs_f + tval_to_f64(&rhs)

    } else if rhs_is_float {
        return rhs_f + tval_to_f64(&lhs)
    }

    lhs_i, lhs_is_int := lhs.(TInt)
    rhs_i, rhs_is_int := rhs.(TInt)

    if lhs_is_int && rhs_is_int {
        return lhs_i + rhs_i
    }

    if lhs_is_int {
        return lhs_i + tval_to_i64(&rhs)
    }

    if rhs_is_int {
        return rhs_i + tval_to_i64(&lhs)
    }

    return tval_to_i64(&lhs) + tval_to_i64(&rhs)
}

eval_sub :: proc(lhs, rhs: TVal) -> TVal {

    lhs := lhs
    rhs := rhs

    _, lhs_is_func := lhs.(TFunc)
    _, rhs_is_func := rhs.(TFunc)
    assert(!lhs_is_func)
    assert(!rhs_is_func)

    lhs_f, lhs_is_float := lhs.(TFloat)
    rhs_f, rhs_is_float := rhs.(TFloat)

    if lhs_is_float && rhs_is_float {
        return lhs_f - rhs_f

    } else if lhs_is_float {
        return lhs_f - tval_to_f64(&rhs)

    } else if rhs_is_float {
        return rhs_f - tval_to_f64(&lhs)
    }

    lhs_i, lhs_is_int := lhs.(TInt)
    rhs_i, rhs_is_int := rhs.(TInt)

    if lhs_is_int && rhs_is_int {
        return lhs_i - rhs_i
    }

    if lhs_is_int {
        return lhs_i - tval_to_i64(&rhs)
    }

    if rhs_is_int {
        return rhs_i - tval_to_i64(&lhs)
    }

    return tval_to_i64(&lhs) - tval_to_i64(&rhs)
}

eval_mul :: proc(lhs, rhs: TVal) -> TVal {

    lhs := lhs
    rhs := rhs

    _, lhs_is_func := lhs.(TFunc)
    _, rhs_is_func := rhs.(TFunc)
    assert(!lhs_is_func)
    assert(!rhs_is_func)

    lhs_f, lhs_is_float := lhs.(TFloat)
    rhs_f, rhs_is_float := rhs.(TFloat)

    if lhs_is_float && rhs_is_float {
        return lhs_f * rhs_f

    } else if lhs_is_float {
        return lhs_f * tval_to_f64(&rhs)

    } else if rhs_is_float {
        return rhs_f * tval_to_f64(&lhs)
    }

    lhs_i, lhs_is_int := lhs.(TInt)
    rhs_i, rhs_is_int := rhs.(TInt)

    if lhs_is_int && rhs_is_int {
        return lhs_i * rhs_i
    }

    if lhs_is_int {
        return lhs_i * tval_to_i64(&rhs)
    }

    if rhs_is_int {
        return rhs_i * tval_to_i64(&lhs)
    }

    return tval_to_i64(&lhs) * tval_to_i64(&rhs)
}

eval_div :: proc(lhs, rhs: TVal) -> TVal {

    lhs := lhs
    rhs := rhs

    _, lhs_is_func := lhs.(TFunc)
    _, rhs_is_func := rhs.(TFunc)
    assert(!lhs_is_func)
    assert(!rhs_is_func)

    lhs_f, lhs_is_float := lhs.(TFloat)
    rhs_f, rhs_is_float := rhs.(TFloat)

    if lhs_is_float && rhs_is_float {
        return lhs_f / rhs_f

    } else if lhs_is_float {
        return lhs_f / tval_to_f64(&rhs)

    } else if rhs_is_float {
        return tval_to_f64(&lhs) / rhs_f
    }

    lhs_i, lhs_is_int := lhs.(TInt)
    rhs_i, rhs_is_int := rhs.(TInt)

    if lhs_is_int && rhs_is_int {
        return lhs_i / rhs_i
    }

    if lhs_is_int {
        return lhs_i / tval_to_i64(&rhs)
    }

    if rhs_is_int {
        return tval_to_i64(&lhs) / rhs_i
    }

    return tval_to_i64(&lhs) / tval_to_i64(&rhs)
}

eval_mod :: proc(lhs, rhs: TVal) -> i64 {
    lhs := lhs; rhs := rhs
    _, lhs_is_func := lhs.(TFunc)
    _, rhs_is_func := rhs.(TFunc)
    assert(!lhs_is_func)
    assert(!rhs_is_func)

    return tval_to_i64(&lhs) % tval_to_i64(&rhs)
}

eval_pow :: proc(lhs, rhs: TVal) -> TVal {

    lhs := lhs
    rhs := rhs

    pow_i64 :: proc(b, e: i64) -> i64 {
        if e == 0 { return 1 }
        res := b
        if e > 0 {
            for i in 1..<e {
                res *= b
            }
        } else if e < 0 {
            iter := abs(e)

            for i in 0..<iter {
                res /= res
                if res == 0 { break }
            }
        }
        return res
    }

     _, lhs_is_func := lhs.(TFunc)
    _, rhs_is_func := rhs.(TFunc)
    assert(!lhs_is_func)
    assert(!rhs_is_func)

    lhs_f, lhs_is_float := lhs.(TFloat)
    rhs_f, rhs_is_float := rhs.(TFloat)

    if lhs_is_float && rhs_is_float {
        return math.pow_f64(lhs_f, rhs_f)

    } else if lhs_is_float {
        return math.pow_f64(lhs_f ,tval_to_f64(&rhs))

    } else if rhs_is_float {
        return math.pow_f64(tval_to_f64(&lhs), rhs_f)
    }

    lhs_i, lhs_is_int := lhs.(TInt)
    rhs_i, rhs_is_int := rhs.(TInt)

    if lhs_is_int && rhs_is_int {
        return pow_i64(lhs_i, rhs_i)
    }

    if lhs_is_int {
        return pow_i64(lhs_i, tval_to_i64(&rhs))
    }

    if rhs_is_int {
        return pow_i64(tval_to_i64(&lhs), rhs_i)
    }

    return pow_i64(tval_to_i64(&lhs), tval_to_i64(&rhs))
}

eval_shl :: proc(lhs, rhs: TVal) -> i64 {
    lhs := lhs
    rhs := rhs
    lhs_i := tval_to_i64(&lhs)
    rhs_i := u64(tval_to_i64(&rhs))

    return lhs_i << rhs_i
}

eval_shr :: proc(lhs, rhs: TVal) -> i64 {
    lhs := lhs
    rhs := rhs
    lhs_i := tval_to_i64(&lhs)
    rhs_i := u64(tval_to_i64(&rhs))

    return lhs_i >> rhs_i
}

eval_and :: proc(lhs, rhs: TVal) -> i64 {

    lhs := lhs
    rhs := rhs
    lhs_i := tval_to_i64(&lhs)
    rhs_i := tval_to_i64(&rhs)

    return lhs_i & rhs_i
}

eval_or :: proc(lhs, rhs: TVal) -> i64 {

    lhs := lhs
    rhs := rhs
    lhs_i := tval_to_i64(&lhs)
    rhs_i := tval_to_i64(&rhs)

    return lhs_i | rhs_i
}

eval_bitwise_not :: proc(v: TVal) -> i64 {
    v := v
    i := tval_to_i64(&v)
    return ~i
}

eval_eq :: proc(lhs, rhs: TVal) -> b64 {

    lhs := lhs
    rhs := rhs

    _, lhs_is_func := lhs.(TFunc)
    _, rhs_is_func := rhs.(TFunc)
    assert(!lhs_is_func)
    assert(!rhs_is_func)

    lhs_f, lhs_is_float := lhs.(TFloat)
    rhs_f, rhs_is_float := rhs.(TFloat)

    if lhs_is_float && rhs_is_float {
        return lhs_f == rhs_f

    } else if lhs_is_float {
        return lhs_f == tval_to_f64(&rhs)

    } else if rhs_is_float {
        return tval_to_f64(&lhs) == rhs_f
    }

    lhs_i, lhs_is_int := lhs.(TInt)
    rhs_i, rhs_is_int := rhs.(TInt)

    if lhs_is_int && rhs_is_int {
        return lhs_i == rhs_i
    }

    if lhs_is_int {
        return lhs_i == tval_to_i64(&rhs)
    }

    if rhs_is_int {
        return tval_to_i64(&lhs) == rhs_i
    }

    return tval_to_i64(&lhs) == tval_to_i64(&rhs)
}

eval_lt :: proc(lhs, rhs: TVal) -> b64 {

    lhs := lhs
    rhs := rhs

    _, lhs_is_func := lhs.(TFunc)
    _, rhs_is_func := rhs.(TFunc)
    assert(!lhs_is_func)
    assert(!rhs_is_func)

    lhs_f, lhs_is_float := lhs.(TFloat)
    rhs_f, rhs_is_float := rhs.(TFloat)

    if lhs_is_float && rhs_is_float {
        return lhs_f < rhs_f

    } else if lhs_is_float {
        return lhs_f < tval_to_f64(&rhs)

    } else if rhs_is_float {
        return tval_to_f64(&lhs) < rhs_f
    }

    lhs_i, lhs_is_int := lhs.(TInt)
    rhs_i, rhs_is_int := rhs.(TInt)

    if lhs_is_int && rhs_is_int {
        return lhs_i < rhs_i
    }

    if lhs_is_int {
        return lhs_i < tval_to_i64(&rhs)
    }

    if rhs_is_int {
        return tval_to_i64(&lhs) < rhs_i
    }

    return tval_to_i64(&lhs) < tval_to_i64(&rhs)
}

eval_lt_eq :: proc(lhs, rhs: TVal) -> b64 {

    lhs := lhs
    rhs := rhs

    _, lhs_is_func := lhs.(TFunc)
    _, rhs_is_func := rhs.(TFunc)
    assert(!lhs_is_func)
    assert(!rhs_is_func)

    lhs_f, lhs_is_float := lhs.(TFloat)
    rhs_f, rhs_is_float := rhs.(TFloat)

    if lhs_is_float && rhs_is_float {
        return lhs_f <= rhs_f

    } else if lhs_is_float {
        return lhs_f <= tval_to_f64(&rhs)

    } else if rhs_is_float {
        return tval_to_f64(&lhs) <= rhs_f
    }

    lhs_i, lhs_is_int := lhs.(TInt)
    rhs_i, rhs_is_int := rhs.(TInt)

    if lhs_is_int && rhs_is_int {
        return lhs_i <= rhs_i
    }

    if lhs_is_int {
        return lhs_i <= tval_to_i64(&rhs)
    }

    if rhs_is_int {
        return tval_to_i64(&lhs) <= rhs_i
    }

    return tval_to_i64(&lhs) <= tval_to_i64(&rhs)
}


new_scope :: proc(interp: ^Interpreter) -> ^Scope {

    prev_scope := interp.current_scope

    new_scope, _ := new(Scope)
    assert(new_scope != nil)

    interp.current_scope = new_scope
    interp.current_scope.level = prev_scope.level + 1
    interp.current_scope.lookup = make(map[string]TVal, 100)
    interp.current_scope.parent = prev_scope

    return interp.current_scope
}

exit_scope :: proc(interp: ^Interpreter) {
    delete(interp.current_scope.lookup)
    interp.current_scope = interp.current_scope.parent
}


eval_expr :: proc(interp: ^Interpreter, expr: ^Expr) -> (TVal, bool) {

    using expr

    switch t in derived_expr {
        case: return TNil{}, false
        case ^ExprBad: return TNil{}, false

        case ^ExprFuncCall: {

            func_call := derived_expr.(^ExprFuncCall)
            func := func_call.func

            if func_ident, func_ident_ok := func.derived_expr.(^ExprIdent); func_ident_ok{
                func_name := func_ident.token.text

                func_expr, func_expr_ok := eval_expr(interp, func_ident)
                if func_expr_ok {

                    func_lit := func_expr.(^ExprFunc)
                    func_proto := func_lit.type
                    func_body  := func_lit.body

                    func_params := func_call.params

                    if len(func_params.list) != len(func_proto.params.list) {
                        report_eval_error(
                            interp,
                            expr.start,
                            "Expecting %d arguments, got %d", len(func_proto.params.list), len(func_params.list)
                        )

                        return TNil{}, false
                    }

                    params_vals := make([dynamic]TVal, 0, 20)
                    defer delete(params_vals)

                    for expr in func_params.list {
                        p_result, p_result_ok := eval_expr(interp, expr)
                        if !p_result_ok {
                            report_eval_error(interp, expr.start, "Error evaluating function parameter")
                            return TNil{}, false
                        }

                        append(&params_vals, p_result)
                    }

                    new_scope(interp)

                    interp.call_stack_ptr += 1
                    cur_call := interp.call_stack_ptr
                    defer if interp.call_stack_ptr == cur_call {
                        interp.call_stack_ptr -= 1
                    }

                    defer exit_scope(interp)

                    for val, index in params_vals {
                        param_name := func_proto.params.list[index].name.derived_expr.(^ExprIdent).token.text
                        interp.current_scope.lookup[string(param_name)] = val
                    }

                    res: TVal = TNil{}
                    res_ok : bool = true

                    res, res_ok = eval_stmt_block_no_new_scope(interp, func_body)

                    return res, res_ok
                } else {
                    report_eval_error(interp, expr.start, "Function %s undefined", func_name)
                    return TNil{}, false
                }

            } else {
                report_eval_error(interp, expr.start, "Function name is not an identifier")
                return TNil{}, false
            }

            return TNil{}, false
        }

        case ^ExprFunc: {
            func_lit := expr.derived_expr.(^ExprFunc)
            return func_lit, true
        }
        case ^ExprProtoFunc: return nil, false

        case ^ExprIdent: {
            ident_tok := derived_expr.(^ExprIdent).token
            ident_name := string(ident_tok.text)

            ident_val : TVal = TNil{}; ident_found := false

            cur_scope := interp.current_scope
            for cur_scope != nil && !ident_found {
                ident_val, ident_found = cur_scope.lookup[ident_name]
                if !ident_found {
                    cur_scope = cur_scope.parent
                }
            }

            if !ident_found {
                report_eval_error(interp, ident_tok.pos, "identifier '%s' undefined", ident_name)
            }

            return ident_val, ident_found
        }

        case ^ExprLiteral: {
            lit_expr := union_to(^ExprLiteral, &derived_expr)
            return eval_literal(interp, lit_expr), true
        }

        case ^ExprUnary: {
            unary := derived_expr.(^ExprUnary)

            expr_val, ok := eval_expr(interp, unary.expr); if !ok {
                report_eval_error(interp, expr.start, "Cannot evaluate expression")
                return expr_val, false
            }

            sign: i64
            switch (unary.op_type) {
                case .Pos, .Neg: {
                    sign = unary.op_type == .Neg ? -1 : 1

                    switch t in expr_val {
                        case TInt   : return union_to(i64, &expr_val) * sign, true
                        case TFloat : return union_to(f64, &expr_val) * f64(sign), true
                        case TBool  : return i64(union_to(b64, &expr_val)) * sign, true
                        case TFunc  : {
                            report_eval_error(interp, expr.start, "Function value not allowed in unary expression")
                            return TNil{}, false
                        }
                        case TNil : {
                            report_eval_error(interp, expr.start, "Cannot evaluate unary expression")
                            return TNil{}, false
                        }
                        case : {
                            report_eval_error(interp, expr.start, "Unsupported unary expression")
                            return TNil{}, false
                        }
                    }
                }

                case .Not: {

                    switch t in expr_val {
                        case TInt, TFloat   : return tval_to_b64(&expr_val), true
                        case TBool          : return !union_to(b64, &expr_val), true
                        case TFunc  : {
                            report_eval_error(interp, expr.start, "Function value not allowed in unary expression")
                            return nil, false
                        }
                        case TNil : {
                            report_eval_error(interp, expr.start, "Cannot evaluate unary expression")
                            return TNil{}, false
                        }
                        case : {
                            report_eval_error(interp, expr.start, "Unsupported unary expression")
                            return nil, false
                        }
                    }
                }

                case .BitwiseNot: {

                    #partial switch t in expr_val {

                        case TFunc  : {
                            report_eval_error(interp, expr.start, "Function value not allowed in unary expression")
                            return TNil{}, false
                        }
                        case TNil : {
                            report_eval_error(interp, expr.start, "Cannot evaluate unary expression")
                            return TNil{}, false
                        }
                        case : {
                            report_eval_error(interp, expr.start, "Unsupported unary expression")
                            return TNil{}, false
                        }
                    }

                    return eval_bitwise_not(expr_val), true
                }

                case: {
                    report_eval_error(interp, expr.start, "Unsupported unary operator")
                    return nil, false
                }
            }
        }

        case ^ExprBinary: {

            bin := derived_expr.(^ExprBinary)

            lhs, lhs_ok := eval_expr(interp, bin.lhs); if !lhs_ok {
                report_eval_error(interp, bin.lhs.start, "Cannot evaluate left hand side expression")
                return lhs, lhs_ok
            }

            rhs, rhs_ok := eval_expr(interp, bin.rhs); if !rhs_ok {
                report_eval_error(interp, bin.rhs.start, "Cannot evaluate right hand side expression")
                return rhs, rhs_ok
            }

            _, lhs_is_func := lhs.(TFunc)
            _, rhs_is_func := rhs.(TFunc)

            if lhs_is_func {
                report_eval_error(interp, bin.lhs.start, "Function value not supported in binary expression")
                return TNil{}, false
            }

            if rhs_is_func {
                report_eval_error(interp, bin.rhs.start, "Function value not supported in binary expression")
                return TNil{}, false
            }

            res: TVal

            switch bin.op_type{

                case .Add:
                    res = eval_add(lhs, rhs)

                case .Sub:
                    res = eval_sub(lhs, rhs)

                case .Mul:
                    res = eval_mul(lhs, rhs)

                case .Div:
                    res = eval_div(lhs, rhs)

                case .Pow:
                    res = eval_pow(lhs, rhs)

                case .Mod:
                    res = eval_mod(lhs, rhs)

                case .Shl:
                    res = eval_shl(lhs, rhs)

                case .Shr:
                    res = eval_shr(lhs, rhs)

                case .And:
                    res = eval_and(lhs, rhs)

                case .Or:
                    res = eval_or(lhs, rhs)

                case .CompEq:
                    res = eval_eq(lhs, rhs)

                case .CompNeq:
                    res = !eval_eq(lhs, rhs)

                case .CompLt:
                    res = eval_lt(lhs, rhs)

                case .CompLtEq:
                    res = eval_lt_eq(lhs, rhs)

                case .CompGt:
                    res = !eval_lt_eq(lhs, rhs)

                case .CompGtEq:
                    res = !eval_lt(lhs, rhs)

                case .CompAnd:
                    res = tval_to_b64(&lhs) && tval_to_b64(&rhs)

                case .CompOr:
                    res = tval_to_b64(&lhs) || tval_to_b64(&rhs)
            }

            return res, true
        }

        case ^ExprList: {
            report_eval_error(interp, expr.start, "Expression list not yet implemented")
            return TNil{}, false
        }
    }

    return TNil{}, true
}

eval_stmt_block_new_scope :: proc(interp: ^Interpreter, stmt: ^Stmt) -> (TVal, bool) {

    stmt_block := stmt.derived_stmt.(^StmtBlock)

    new_scope(interp)
    defer exit_scope(interp)

    res : TVal = TNil{}; res_ok := true
    start_call_ptr := interp.call_stack_ptr
    for stmt in stmt_block.stmts {
        res, res_ok = eval_stmt(interp, stmt)
        if !res_ok || interp.call_stack_ptr < start_call_ptr {
            break
        }
    }

    return res, res_ok
}

eval_stmt_block_no_new_scope :: proc(interp: ^Interpreter, stmt: ^Stmt) -> (TVal, bool) {

    stmt_block := stmt.derived_stmt.(^StmtBlock)

    res : TVal = TNil{}; res_ok := true
    start_call_ptr := interp.call_stack_ptr
    for stmt in stmt_block.stmts {
        res, res_ok = eval_stmt(interp, stmt)
        if !res_ok || interp.call_stack_ptr < start_call_ptr {
            break
        }
    }

    return res, res_ok
}

eval_stmt :: proc (interp: ^Interpreter, stmt: ^Stmt) -> (TVal, bool) {

    #partial switch t in stmt.derived_stmt {
        case ^StmtBad: {
            report_eval_error(interp, stmt.start, "Bad statement")
            return TNil{}, false
        }

        case ^StmtAssign: {

            stmt_assign := union_to(^StmtAssign, &stmt.derived_stmt)

            if val, ok := eval_expr(interp, stmt_assign.val); ok {

                // TODO: proper scope handling of variable
                //       currently this only works for variable
                //       that was instantiated in the current scope.
                //       assinging to variables from the parent scope
                //       doesn't work.
                ident := stmt_assign.lhs.derived_expr.(^ExprIdent)
                name := string(ident.token.text)
                exists := name in interp.current_scope.lookup

                #partial switch stmt_assign.op.type {
                    case: assert(false)
                    case .Eq:
                    case .AddEq: {
                        if !exists {
                            report_eval_error(
                                interp,
                                ident.start,
                                "identifier '%s' is undefined",
                                name,
                            )
                            return TNil{}, false
                        }
                        ident_val := interp.current_scope.lookup[name]
                        val = eval_add(ident_val, val)
                    }

                    case .SubEq: {
                        if !exists {
                            report_eval_error(
                                interp,
                                ident.start,
                                "identifier '%s' is undefined",
                                name,
                            )
                            return TNil{}, false
                        }
                        ident_val := interp.current_scope.lookup[name]
                        val = eval_sub(ident_val, val)
                    }
                    case .MulEq: {
                        if !exists {
                            report_eval_error(
                                interp,
                                ident.start,
                                "identifier '%s' is undefined",
                                name,
                            )
                            return TNil{}, false
                        }
                        ident_val := interp.current_scope.lookup[name]
                        val = eval_mul(ident_val, val)
                    }
                    case .DivEq: {
                        if !exists {
                            report_eval_error(
                                interp,
                                ident.start,
                                "identifier '%s' is undefined",
                                name,
                            )
                            return TNil{}, false
                        }
                        ident_val := interp.current_scope.lookup[name]
                        val = eval_div(ident_val, val)
                    }
                    case .ModEq: {
                        if !exists {
                            report_eval_error(
                                interp,
                                ident.start,
                                "identifier '%s' is undefined",
                                name,
                            )
                            return TNil{}, false
                        }
                        ident_val := interp.current_scope.lookup[name]
                        val = eval_mod(ident_val, val)
                    }
                    case .ShlEq: {
                        if !exists {
                            report_eval_error(
                                interp,
                                ident.start,
                                "identifier '%s' is undefined",
                                name,
                            )
                            return TNil{}, false
                        }
                        ident_val := interp.current_scope.lookup[name]
                        val = eval_shl(ident_val, val)
                    }
                    case .ShrEq: {
                        if !exists {
                            report_eval_error(
                                interp,
                                ident.start,
                                "identifier '%s' is undefined",
                                name,
                            )
                            return TNil{}, false
                        }
                        ident_val := interp.current_scope.lookup[name]
                        val = eval_shr(ident_val, val)
                    }

                    case .AndEq: {
                        if !exists {
                            report_eval_error(
                                interp,
                                ident.start,
                                "identifier '%s' is undefined",
                                name,
                            )
                            return TNil{}, false
                        }
                        ident_val := interp.current_scope.lookup[name]
                        val = eval_and(ident_val, val)
                    }
                    case .OrEq: {
                        if !exists {
                            report_eval_error(
                                interp,
                                ident.start,
                                "identifier '%s' is undefined",
                                name,
                            )
                            return TNil{}, false
                        }
                        ident_val := interp.current_scope.lookup[name]
                        val = eval_or(ident_val, val)
                    }
                }

                interp.current_scope.lookup[name] = val
                return val, true
            } else {
                report_eval_error(interp, stmt_assign.val.start, "Cannot evaluate expression in statement")
                return TNil{}, false
            }
        }

        case ^StmtEval: {

            stmt_eval := union_to(^StmtEval, &stmt.derived_stmt)
            return eval_expr(interp, stmt_eval.expr)
        }

        case ^StmtIf: {

            stmt_if := union_to(^StmtIf, &stmt.derived_stmt)

            if pred_val, pred_ok := eval_expr(interp, stmt_if.pred); pred_ok {
                pred_val := tval_to_b64(&pred_val)

                if pred_val {
                    return eval_stmt(interp, stmt_if.body)
                } else if stmt_if.else_stmt != nil {
                    return eval_stmt(interp, stmt_if.else_stmt)
                }

            } else {
                report_eval_error(interp, stmt_if.start, "Cannot evaluate if statement's predicate")
                return TNil{}, false
            }
        }

        case ^StmtPrint: {
            stmt_print := union_to(^StmtPrint, &stmt.derived_stmt)
            if print_val, print_ok := eval_expr(interp, stmt_print.expr); print_ok {
                if stmt_print.ident_name != "" {
                    fmt.printf(">> %s = %v\n", stmt_print.ident_name, print_val)
                } else {
                    fmt.printf(">> %v\n", print_val)
                }

                return TNil{}, true
            } else {
                report_eval_error(interp, stmt_print.start, "Cannot evaluate print statement")
                return TNil{}, false
            }
        }

        case ^StmtReturn: {

            stmt_return := union_to(^StmtReturn, &stmt.derived_stmt)
            interp.call_stack_ptr -= 1
            return eval_expr(interp, stmt_return.res)
        }

        case ^StmtBlock: {
            return eval_stmt_block_new_scope(interp, stmt)
        }
    }

    return TNil{}, true

}


init_interpreter :: proc (interp: ^Interpreter, file_path: string, file_data: []byte) {

    file_node := new_node(File, TokenPos{ file = file_path }, TokenPos{ file = file_path })
    file_node.path = file_path
    file_node.stmts = make([dynamic]^Stmt, 0, 1024)
    file_node.data = file_data

    interp.current_file = file_node

    interp.file_scope.lookup = make(map[string]TVal, 512)
    interp.current_scope = &interp.file_scope

}

destroy_interpreter :: proc (interp: ^Interpreter) {
    delete(interp.file_scope.lookup)
    delete(interp.current_file.stmts)
}

eval_literal :: proc (interp: ^Interpreter, lit: ^ExprLiteral) -> TVal {
    lit_tok := lit.token
    #partial switch lit_tok.type {
        case .Integer: {
            v := int_from_bytes(lit_tok.text)
            return v
        }

        case .Float: {
            v := float_from_bytes(lit_tok.text)
            return v
        }

        case .BTrue: {
            return true
        }

        case .BFalse: {
            return false
        }

        case : {
            assert(false)
        }
    }

    return TNil{}
}

is_in :: proc (a: []int, e: int) -> bool {
    if len(a) == 0 {
        return false
    }

    for i in a {
        if e == i {
            return true
        }
    }

    return false
}

_where_assign_stmt :: proc(interp: ^Interpreter, ident_name: []byte) -> (int, bool) {

    at := 0
    found := false
    for stmt, stmt_index in interp.current_file.stmts {
        if stmt_assign, is_assign := stmt.derived_stmt.(^StmtAssign); is_assign {
            assign_to_name := stmt_assign.lhs.derived_expr.(^ExprIdent).token.text
            if string(assign_to_name) == string(ident_name) {
                at = stmt_index
                found = true
                break
            }
        }
    }

    return at, found
}

_update_exec :: proc(
    interp: ^Interpreter,
    expr: ^Expr,
    cur_stmt_index: int,
    exec_order: ^[dynamic]int,
) -> bool {

    derived := expr.derived_expr

    exec := cur_stmt_index
    found := true
    #partial switch t in derived {
        case ^ExprIdent: {
            ident := union_to(^ExprIdent, &derived)
            ident_name := ident.token.text

            exec, found = _where_assign_stmt(interp, ident_name)
            stmt := interp.current_file.stmts[exec]
            stmt_assign := stmt.derived_stmt.(^StmtAssign)
            _update_exec(interp, stmt_assign.val, exec, exec_order)

            if !found {
                report_eval_error(interp, ident.start, "Identifier '%s' undefined", ident_name)
            }
        }

        case ^ExprBinary: {
            expr_bin := union_to(^ExprBinary, &derived)
            found = _update_exec(interp, expr_bin.rhs, cur_stmt_index, exec_order)
        }

        case ^ExprUnary: {
            expr_unary := union_to(^ExprUnary, &derived)
            found = _update_exec(interp, expr_unary.expr, cur_stmt_index, exec_order)
        }

        case ^ExprFuncCall: {
            expr_call := union_to(^ExprFuncCall, &derived)

            found = _update_exec(interp, expr_call.func, cur_stmt_index, exec_order)

            if found {
                loop: for expr in expr_call.params.list {
                    found = _update_exec(interp, expr, cur_stmt_index, exec_order)
                    if !found { break loop }
                }
            }
        }
    }
    if !is_in((exec_order^)[:], exec) {
        append(exec_order, exec)
    }

    return found
}

update_stmt_exec_order :: proc(
    interp: ^Interpreter,
    stmt: ^Stmt,
    stmt_index: int,
    exec_order: ^[dynamic]int,
) -> bool {
    derived := stmt.derived_stmt

    ok := true
    #partial switch t in derived {
        case ^StmtAssign: {
            stmt_assign := union_to(^StmtAssign, &derived)
            ok = _update_exec(interp, stmt_assign.val, stmt_index, exec_order)

        }

        case ^StmtEval: {
            stmt_eval := union_to(^StmtEval, &derived)
            ok = _update_exec(interp, stmt_eval.expr, stmt_index, exec_order)
        }

        case ^StmtPrint: {
            stmt_print := union_to(^StmtPrint, &derived)
            ok = _update_exec(interp, stmt_print.expr, stmt_index, exec_order)
        }
    }

    if !is_in((exec_order^)[:], stmt_index) {
        append(exec_order, stmt_index)
    }

    if !ok {
        report_eval_error(interp, stmt.start, "Cannot resolve statement execution order")
    }

    return ok
}

is_simple_expr :: proc(expr: ^Expr) -> bool {
    res := false

    #partial switch t in expr.derived_expr {
        case ^ExprLiteral, ^ExprFunc: {
            res = true
        }

        case ^ExprUnary: {
            unary := union_to(^ExprUnary, &expr.derived_expr)
            res = is_simple_expr(unary.expr)
        }

        case ^ExprBinary: {
            binary := union_to(^ExprBinary, &expr.derived_expr)

            res = is_simple_expr(binary.lhs) && is_simple_expr(binary.rhs)
        }
    }

    return res
}

parse_then_run_file :: proc (interp: ^Interpreter) -> bool {
    file_ok := parse_file(&interp.parser, interp.current_file)

    if !file_ok {
        return file_ok
    }

    exec_order := make([dynamic]int, 0, len(interp.current_file.stmts), context.temp_allocator)

    for stmt, stmt_index in interp.current_file.stmts {
        if assign, is_assign := stmt.derived_stmt.(^StmtAssign); is_assign {
            assign_val := assign.val

            if is_simple_expr(assign_val) {
                append(&exec_order, stmt_index)
            }
        }
    }

    exec_order_ok : bool
    for stmt, stmt_index in interp.current_file.stmts {
        exec_order_ok = update_stmt_exec_order(interp, stmt, stmt_index, &exec_order)
        if !exec_order_ok { break }
    }

    if !exec_order_ok {
        for i := interp.error_count - 1; i > 0; i -= 1{
            fmt.print(interp.errors[i].err_msg)
        }
        return false
    }

    for stmt_index in exec_order {
        stmt_index := stmt_index
        stmt := interp.current_file.stmts[stmt_index]

        _, stmt_ok := eval_stmt(interp, stmt)
        if !stmt_ok {
            break
        }
    }

    for i := interp.error_count - 1; i > 0; i -= 1{
        fmt.print(interp.errors[i].err_msg)
    }

    return file_ok
}
