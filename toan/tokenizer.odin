package toan

import "core:fmt"

TokenType :: enum {
    Invalid,
    Eos,
    Comment,

    // Keywords
    KeywordBegin,
        Return,
        If,
        Else,
        Print,
        PrintIdent,
    KeywordEnd,


    LiteralBegin,
        Identifier,
        Integer,
        Float,
        BTrue,
        BFalse,
        Func,
    LiteralEnd,

    OperatorBegin,

        EqOpBegin,
            Eq,
            AddEq,
            SubEq,
            MulEq,
            DivEq,
            ModEq,
            ShlEq,
            ShrEq,
            AndEq,
            OrEq,
        EqOpEnd,

        Add,
        Sub,
        Mul,
        Div,
        Mod,
        Hat,
        And,
        Or,
        Shl,
        Shr,

        CompOr,
        CompAnd,
        CompEq,
        CompNeq,
        CompLt,
        CompLtEq,
        CompGt,
        CompGtEq,

        Excl,
        Tilde,

        OpenParen,
        CloseParen,
        OpenBrace,
        CloseBrace,
        Comma,

    OperatorEnd,

}

TokenPos :: struct {
    file: string,
    line, col: int,
}

Token :: struct {
    type: TokenType,
    text: []byte,
    using pos: TokenPos,
}


Tokenizer :: struct {
    offset: int,
    stream: []byte,
    file: string,
    line, col: int,
    error_count: int,
}


report_token_error :: proc(tokenizer: ^Tokenizer, _fmt: string, args: ..any) {
    tokenizer.error_count += 1
    fmt.printf("%s(%d:%d) ", tokenizer.file, tokenizer.line, tokenizer.col)
    fmt.printf(_fmt, ..args)
    fmt.printf("\n")
}

is_whitespace :: #force_inline proc (b: []byte) -> int {
    return int(b[0] == ' ' || b[0] == '\v' || b[0] == '\t')
}

is_newline :: #force_inline proc (b: []byte) -> int {

    if b[0] == '\n' {
        return 1
    } else if len(b) >= 2 && b[0] == '\r' && b[1] == '\n' {
        return 2
    }

    return 0
}

is_whitespace_or_newline :: #force_inline proc (b: []byte) -> int {

    ws := is_whitespace(b)
    if ws > 0 {
        return ws
    }

    nl := is_newline(b)
    return nl
}

is_digit :: #force_inline proc (b: u8) -> bool {
    return '0' <= b && b <= '9'
}

eat_all_whitespace_or_newline :: #force_inline proc (tokenizer: ^Tokenizer) {

    at := tokenizer.stream[tokenizer.offset:]

    for tokenizer.offset < len(tokenizer.stream) {
        is_nl := is_newline(at)
        is_ws := is_whitespace(at)

        if is_nl > 0 {
            tokenizer.line += 1
            tokenizer.col = 0
        }

        tokenizer.offset += (is_nl + is_ws)
        tokenizer.col += (is_nl + is_ws)

        at = tokenizer.stream[tokenizer.offset:]

        if is_nl == 0 && is_ws == 0 {
            break
        }
    }
}

int_from_bytes :: proc(buf: []byte) -> i64 {
    res : i64 = 0

    for b in buf {
        if b != '_' {
            res = res * 10 + i64(b - '0')
        }
    }

    return res
}

float_from_bytes :: proc(buf: []byte) -> f64 {
    res : f64 = 0

    dot_at := -1

    for b, index in buf {
        if b == '.' {
            dot_at = index
            break
        }
        if b != '_' {
            res = res * 10 + f64(b - '0')
        }
    }

    if dot_at >= 0 {
        pow10 : f64 = 10
        for b in buf[dot_at + 1:] {
            if b != '_' {
                res += f64(b - '0') / pow10
                pow10 *= 10
            }
        }
    }

    return res
}

get_token :: proc(tokenizer: ^Tokenizer) -> Token {

    token := Token{}
    token.line = tokenizer.line
    token.col  = tokenizer.col
    token.file = tokenizer.file

    if tokenizer.offset >= len(tokenizer.stream) {
        token.type = .Eos
        return token
    }

    eat_all_whitespace_or_newline(tokenizer)

    if tokenizer.line == 0 {
        tokenizer.line = 1
        tokenizer.col = 1
    }

    token.line = tokenizer.line
    token.col = tokenizer.col

    if tokenizer.offset >= len(tokenizer.stream) {
        token.type = .Eos
        return token
    }

    token_begin := tokenizer.offset
    str_len := 0

    is_op :: proc(c: byte) -> bool {
        return c == '(' || c == ')' ||
               c == '{' || c == '}' ||
               c == '+' || c == '-' ||
               c == '*' || c == '/' ||
               c == '%' || c == '^' ||
               c == '=' || c == '>' ||
               c == '!' || c == '<' ||
               c == ',' || c == '&' ||
               c == '|'
     }

     is_cur_byte :: proc(tok: ^Tokenizer, comp_byte: byte) -> bool {
         return tok.offset < len(tok.stream) && tok.stream[tok.offset] == comp_byte
     }


    switch tokenizer.stream[tokenizer.offset] {
        case: {

            at := tokenizer.stream[tokenizer.offset:]

            if is_digit(at[0]) {

                has_dot := false
                is_err := false

                for tokenizer.offset < len(tokenizer.stream) &&
                    is_whitespace_or_newline(at) == 0 &&
                    !is_op(at[0])
                {
                    if !is_digit(at[0]) {
                        if at[0] == '.' && has_dot {
                            is_err = true
                            report_token_error(tokenizer, "Bad floating point number, more than 1 dot")
                        } else if at[0] == '.' {
                            has_dot = true
                        } else if at[0] != '_' {
                            is_err = true
                            report_token_error(tokenizer, "Non digit character in number")
                        }
                    }

                    tokenizer.offset += 1
                    tokenizer.col += 1
                    str_len += 1
                    at = tokenizer.stream[tokenizer.offset:]
                }

                if !is_err {
                    if has_dot {
                        token.type = .Float
                    } else {
                        token.type = .Integer
                    }
                } else {
                    token.type = .Invalid
                }

            } else {
                for tokenizer.offset < len(tokenizer.stream) &&
                    is_whitespace_or_newline(at) == 0 &&
                    !is_op(at[0])
                {
                    tokenizer.offset += 1
                    tokenizer.col += 1
                    str_len += 1
                    at = tokenizer.stream[tokenizer.offset:]
                }

                token.type = .Identifier
            }
        }

        case '#': {

            at := tokenizer.stream[tokenizer.offset:]

            for tokenizer.offset < len(tokenizer.stream) && is_newline(at) == 0 {
                tokenizer.offset += 1
                tokenizer.col += 1
                str_len += 1
                at = tokenizer.stream[tokenizer.offset:]
            }

            token.type = .Comment
        }

        case '=': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .Eq
            str_len = 1

            if is_cur_byte(tokenizer, '=') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .CompEq
                str_len += 1
            } else if is_cur_byte(tokenizer, '>') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .Return
                str_len += 1
            }
        }

        case '+': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .Add
            str_len = 1

            if is_cur_byte(tokenizer, '=') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .AddEq
                str_len = 1
            }
        }

        case '-': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .Sub
            str_len = 1

            if is_cur_byte(tokenizer, '=') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .SubEq
                str_len = 1
            }
        }

        case '*': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .Mul
            str_len = 1

            if is_cur_byte(tokenizer, '=') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .MulEq
                str_len = 1
            }
        }

        case '/': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .Div
            str_len = 1

            if is_cur_byte(tokenizer, '=') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .DivEq
                str_len = 1
            }
        }

        case '%': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .Mod
            str_len = 1

            if is_cur_byte(tokenizer, '=') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .ModEq
                str_len = 1
            }
        }

        case '^': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .Hat
            str_len = 1
        }

        case '>': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .CompGt
            str_len = 1

            if is_cur_byte(tokenizer, '=') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .CompGtEq
                str_len += 1
            } else if is_cur_byte(tokenizer, '>') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .Shr
                str_len += 1

                if is_cur_byte(tokenizer, '=') {
                    tokenizer.offset += 1
                    tokenizer.col += 1
                    token.type = .ShrEq
                    str_len += 1
                }
            }
        }

        case '<': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .CompLt
            str_len = 1

            if is_cur_byte(tokenizer, '=') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .CompLtEq
                str_len += 1
            } else if is_cur_byte(tokenizer, '<') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .Shl
                str_len += 1

                if is_cur_byte(tokenizer, '=') {
                    tokenizer.offset += 1
                    tokenizer.col += 1
                    token.type = .ShrEq
                    str_len += 1
                }
            }
        }

        case '!': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .Excl
            str_len = 1

            if is_cur_byte(tokenizer, '=') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .CompNeq
                str_len += 1
            }
        }

        case '(': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .OpenParen
            str_len = 1
        }

        case ')': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .CloseParen
            str_len = 1
        }

        case '{': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .OpenBrace
            str_len = 1
        }

        case '}': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .CloseBrace
            str_len = 1

        }

        case ',': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .Comma
            str_len = 1
        }

        case '~' : {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .Tilde
        }

        case '&': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .And
            str_len += 1

            if is_cur_byte(tokenizer, '&') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .CompAnd
                str_len += 1
            } else if is_cur_byte(tokenizer, '=') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .AndEq
                str_len += 1
            }
        }

        case '|': {
            tokenizer.offset += 1
            tokenizer.col += 1
            token.type = .Or
            str_len += 1

            if is_cur_byte(tokenizer, '|') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .CompOr
                str_len += 1
            } else if is_cur_byte(tokenizer, '=') {
                tokenizer.offset += 1
                tokenizer.col += 1
                token.type = .OrEq
                str_len += 1
            }
        }
    }

    token.text = tokenizer.stream[token_begin:token_begin + str_len]

    if token.type == .Identifier {

        text := string(token.text)

        switch text {
            case "fn": token.type = .Func
            case "true": token.type = .BTrue
            case "false": token.type = .BFalse
            case "if": token.type = .If
            case "else": token.type = .Else
            case "print": token.type = .Print
            case "printv": token.type = .PrintIdent
            case:
        }
    }

    return token
}

is_token_keyword :: #force_inline proc (token: Token) -> bool {
    return .KeywordBegin <= token.type && token.type <= .KeywordEnd
}

is_token_literal :: #force_inline proc (token: Token) -> bool {
    return .LiteralBegin <= token.type && token.type <= .LiteralEnd
}

is_token_operator :: #force_inline proc (token: Token) -> bool {
    return .OperatorBegin <= token.type && token.type <= .OperatorEnd
}

is_token_binary :: #force_inline proc (token: Token) -> bool {
    return (
        token.type == .Add || token.type == .Sub ||
        token.type == .Mul || token.type == .Div ||
        token.type == .Mod || token.type == .Hat
    )
}

token_pos_eq :: #force_inline proc (p1, p2: TokenPos) -> bool {
    return p1.file == p2.file && p1.line == p2.line && p1.col == p2.col
}
