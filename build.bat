@echo off

SETLOCAL

set debug_build_path=builds\debug\

if not exist %debug_build_path%\ (
    echo "creating build directory %debug_build_path%"
    mkdir %debug_build_path%
)

cd %debug_build_path%

odin build ../../ -debug -o:minimal -out:toan.exe -pdb-name:toan.pdb

ENDLOCAL
