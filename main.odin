package main

import "core:os"
import "core:fmt"
import "toan"

Interpreter         :: toan.Interpreter
init_interpreter    :: toan.init_interpreter
destroy_interpreter :: toan.destroy_interpreter
parse_then_run_file :: toan.parse_then_run_file



main :: proc () {
    if len(os.args) < 2 {
        fmt.printf("Error: need a file path for the first argument\n")
        os.exit(1)
    }
    if len(os.args[1]) == 0 {
        fmt.printf("Error: need a non-empty string for file path in the first argument\n")
        os.exit(1)
    }

    filename := os.args[1]

    if file_data, ok := os.read_entire_file_from_filename(filename); ok {
        if len(file_data) == 0 {
            fmt.printf("Error: file %v is empty\n", filename)
            os.exit(1)
        }

        interp := Interpreter{}
        init_interpreter(&interp, filename, file_data)
        defer destroy_interpreter(&interp)

        if parse_then_run_file(&interp) {
            fmt.printf("====== Run ok ======\n")
        } else {
            fmt.printf("====== Run failed =======")
        }


    } else {
        fmt.printf("Error: can't read file %v\n", filename)
        os.exit(1)
    }
}
